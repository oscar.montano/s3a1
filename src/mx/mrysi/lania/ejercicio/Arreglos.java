/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.mrysi.lania.ejercicio;

/**
 *
 * @author oskar
 */
public class Arreglos {
    public int[] calcularPersonasTiempoParaArreglos(int n){
        int[] resultado = new int[2];
//        En un servicio de preparación de arreglos florales se suelen atender 
//        servicios de diferentes tamaños según las siguientes consideraciones:
//        La preparación de cada arreglo requiere 15 minutos
//        Si se piden de 1 a 19 arreglos, se asigna una persona a ese encargo
        if (n >= 1 && n <= 19) {
            resultado[0] = 1;
        }
//        Si se piden de 20 a 49 arreglos, se asignan dos personas a ese encargo
        else if (n >= 20 && n <= 49) {
            resultado[0] = 2;
        }
//        Si se piden de 50 arreglos en adelante, se asigna una persona 
//        por cada 25 arreglos y una persona extra. Por ejemplo 50 arreglos o 60 arreglos 
//        serían atendidos por un equipo de tres personas pero 75 arreglos se harían entre 4 personas
        else if (n >= 50) {
            resultado[0] = 2 + (int) Math.ceil(((double) n - 25.0) / 25.0);
        }
        
        //tiempo = (arreglos * minutos) / personas
        if (n > 0){
            resultado[1] = (n * 15) / resultado[0];
        }
        
        System.out.println("------------------------------------");
        System.out.println("Arreglos: " + n);
        System.out.println("Personas asignadas: " + resultado[0]);
        System.out.println("Tiempo: " + resultado[1] + " minutos");

        return resultado;
    }
}
