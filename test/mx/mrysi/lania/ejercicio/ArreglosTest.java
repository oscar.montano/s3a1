/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.mrysi.lania.ejercicio;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author oskar
 */
public class ArreglosTest {
    
    @Test
    public void testCeroArreglos() {
        int n = 0;
        Arreglos instance = new Arreglos();
        int[] expResult = new int[2];
        int[] result = instance.calcularPersonasTiempoParaArreglos(n);
        assertArrayEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }
    
    @Test
    public void testDoceArreglos() {
        int n = 12;
        Arreglos instance = new Arreglos();
        int[] expResult = new int[]{ 1, (12 * 15) };
        int[] result = instance.calcularPersonasTiempoParaArreglos(n);
        assertArrayEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }
    
    @Test
    public void testTreintaytresArreglos() {
        int n = 33;
        Arreglos instance = new Arreglos();
        int[] expResult = new int[]{ 2, ((33 * 15) / 2) };
        int[] result = instance.calcularPersonasTiempoParaArreglos(n);
        assertArrayEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }
    
    @Test
    public void testCincuentaArreglos() {
        int n = 50;
        Arreglos instance = new Arreglos();
        int[] expResult = new int[]{ 3, ((50 * 15) / 3) };
        int[] result = instance.calcularPersonasTiempoParaArreglos(n);
        assertArrayEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }
    
    @Test
    public void testOchentaArreglos() {
        int n = 80;
        Arreglos instance = new Arreglos();
        int[] expResult = new int[]{ 5, ((80 * 15) / 5) };
        int[] result = instance.calcularPersonasTiempoParaArreglos(n);
        assertArrayEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }
    
    @Test
    public void testArreglosNegativos() {
        int n = -6;
        Arreglos instance = new Arreglos();
        int[] expResult = new int[2];
        int[] result = instance.calcularPersonasTiempoParaArreglos(n);
        assertArrayEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }
    
}
